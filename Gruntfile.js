module.exports = function (grunt) {

    var jsFiles = [
        'assets/js/jquery.min.js',
		'assets/js/bootstrap.min.js',
		'assets/js/jquery-plugin-collection.js',
		'assets/js/script.js'
    ];
    var cssFiles = [
        'assets/css/themify-icons.css',
        'assets/css/flaticon.css',
        'assets/css/bootstrap.min.css',
        'assets/css/animate.css',
        'assets/css/owl.carousel.css',
        'assets/css/owl.theme.css',
        'assets/css/slick.css',
        'assets/css/slick-theme.css',
        'assets/css/swiper.min.css',
        'assets/css/owl.transitions.css',
		'assets/css/jquery.fancybox.css',
		'assets/css/magnific-popup.css',
		'assets/css/style.css'
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'assets/js/',
        cssDistDir: 'assets/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};
