        <!-- start site-footer -->
        <footer class="site-footer footer-style-1">
            <div class="inner">
                <div class="couple-pic">
                    <img src="<?php echo base_url();?>assets/images/footer-couple-pic.jpg" alt>
                </div>
                <h2>Thank you</h2>
                <ul class="social-links">
                    <li><a href="index.html#"><i class="ti-facebook"></i></a></li>
                    <li><a href="index.html#"><i class="ti-twitter-alt"></i></a></li>
                    <li><a href="index.html#"><i class="ti-linkedin"></i></a></li>
                    <li><a href="index.html#"><i class="ti-pinterest"></i></a></li>
                </ul>
                <p>Copyright 2020, Made with Luv by <a href="http://www.redystem.id/">BondanNoviada</a></p>
            </div>
        </footer>
        <!-- end site-footer -->


    </div>
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="<?php echo base_url();?>assets/js/bondan-sanis.min.js"></script>
</body>
</html>
